clear all;
close all;
clc;

%% LOAD DATA
filename = 'CaporasoF4';
load(['CompiledData/',filename,'.mat']);

relabus_dat = relabus;
[relabus_dat,tress,trets,mtres,mtret,tress_pool,trets_pool,...
    prevs,abunds_ml,stds_ml,abunds_lm,vars_lm,slopes_log,mus] =...
    ProcessRelabu(relabus_dat);
[N,steps] = size(relabus_dat);

pwcs_dat = zeros(N);
for i = 1:N
    for j = 1:N
        xthis = relabus_dat(i,:);
        ythis = relabus_dat(j,:);
        xthis = log10(xthis);
        ythis = log10(ythis);
        inds = isinf(xthis) | isnan(xthis) | isinf(ythis) | isnan(ythis);
        xthis(inds) = [];
        ythis(inds) = [];
        
        if length(xthis)>5
            pwcs_dat(i,j) = corr(xthis',ythis');
        else
            pwcs_dat(i,j) = NaN;
        end
    end
end

%% LOAD SIMS
N = 50;
M = 30;
S = 0.1;
sig = 0.2;
k = 0.8 ;
reps = 20;
inds_best = [3,1,5,8];

mus_pooled = [];
abunds_lm_pooled = [];
vars_lm_pooled = [];
abunds_ml_pooled = [];
stds_ml_pooled = [];
prevs_pooled = [];
slopes_log_pooled = [];
pwcs_sim_pooled = [];
rich_pooled = [];
for iii = 1:reps
    filename = ['201126_',num2str(N),...
        '_',num2str(M),...
        '_',num2str(S),...
        '_',num2str(sig),...
        '_',num2str(k),...
        '_',num2str(iii),'.mat'];
    load(['SimsForPlots/',filename],'xss');
    relabus_sim = xss';
    [relabus_sim,tress_sim,trets_sim,mtres_sim,mtret_sim,...
        tress_pool_sim,trets_pool_sim,...
        prevs_sim,abunds_ml_sim,stds_ml_sim,...
        abunds_lm_sim,vars_lm_sim,slopes_log_sim,mus_sim] =...
        ProcessRelabu(relabus_sim);
    mus_pooled = [mus_pooled,mus_sim];
    abunds_lm_pooled = [abunds_lm_pooled,abunds_lm_sim];
    vars_lm_pooled = [vars_lm_pooled,vars_lm_sim];
    abunds_ml_pooled = [abunds_ml_pooled,abunds_ml_sim];
    stds_ml_pooled = [stds_ml_pooled,stds_ml_sim];
    prevs_pooled = [prevs_pooled,prevs_sim];
    slopes_log_pooled = [slopes_log_pooled,slopes_log_sim];
    rich_pooled = [rich_pooled,sum(relabus_sim>0,1)'];
    
    pwcs_sim = zeros(N);
    for i = 1:N
        for j = 1:N
            xthis = relabus_sim(i,:);
            ythis = relabus_sim(j,:);
            xthis = log10(xthis);
            ythis = log10(ythis);
            inds = isinf(xthis) | isnan(xthis) | isinf(ythis) | isnan(ythis);
            xthis(inds) = [];
            ythis(inds) = [];

            if length(xthis)>5
                pwcs_sim(i,j) = corr(xthis',ythis');
            else
                pwcs_sim(i,j) = NaN;
            end
        end
    end
    pwcs_sim_pooled = [pwcs_sim_pooled,pwcs_sim];
end

load('201202_all.mat');
N = 50;
Ms = [10,20,30,40,50,100,150,200,250];
Ss = 0.1:0.1:0.9;
sigs = [0.02,0.05,0.1,0.15,0.2,0.3,0.4,0.5];
ks = 0.1:0.1:1;
m1 = length(Ms);
m2 = length(Ss);
m3 = length(sigs);
m4 = length(ks);

%% PAIRWISE CORRELATIONS
figthis = figure;
hold on;
box on;
ax = gca;
ax.ActivePositionProperty = 'position';

xbins = -1:0.05:1;
xtemp = pwcs_dat(:);
xtemp(isnan(xtemp) | xtemp==1) = [];
histogram(xtemp,xbins,'normalization','pdf',...
    'edgecolor','none','facealpha',0.4);

pwcs_counts = zeros(length(xbins)-1,reps);
for iii = 1:reps
    xthis = pwcs_sim_pooled(:,((iii-1)*50+1):(iii)*50);
    xthis = xthis(:);
    xthis(xthis==1) = [];
    hthis = histcounts(xthis,xbins,'normalization','pdf');
    pwcs_counts(:,iii) = hthis;
end
ymean = nanmean(pwcs_counts,2);
yerrs = nanstd(pwcs_counts,[],2);
xthis = (xbins(1:end-1)+xbins(2:end))./2;
ycnst = sum(ymean)*mean(diff(xbins));
ymean = ymean./ycnst;
yerrs = yerrs./ycnst;

xthis = [xbins(1),xthis(2:end-1),xbins(end)];
plot(xthis,ymean,'-','linewidth',1.5,'color',ax.ColorOrder(2,:));
xthis = xthis';
xC = [xthis; flipud(xthis)];
yC = [ymean+yerrs; flipud(ymean-yerrs)];
inds = yC<=0;
xC(inds) = [];
yC(inds) = [];
fill(xC, yC, ax.ColorOrder(2,:),...
    'FaceAlpha',0.2,'linestyle','none');

pwcs_sim_pooled1 = pwcs_sim_pooled;

load('pwcs_null.mat');
reps = 5;
pwcs_counts = zeros(length(xbins)-1,reps);
for iii = 1:reps
    xthis = pwcs_sim_pooled(:,((iii-1)*117+1):(iii)*117);
    xthis = xthis(:);
    xthis(xthis==1) = [];
    hthis = histcounts(xthis,xbins,'normalization','pdf');
    pwcs_counts(:,iii) = hthis;
end
ymean = nanmean(pwcs_counts,2);
yerrs = nanstd(pwcs_counts,[],2);
xthis = (xbins(1:end-1)+xbins(2:end))./2;
ycnst = sum(ymean)*mean(diff(xbins));
ymean = ymean./ycnst;
yerrs = yerrs./ycnst;

xthis = [xbins(1),xthis(2:end-1),xbins(end)];
plot(xthis,ymean,':','linewidth',1.5,'color','k');

xlabel('Correlation');
ylabel('PDF');
xlim([-1,1]);
ylim([0,2]);

set(gca,'linewidth',1,'ticklength',[0.02,0.01]);
set(gcf,'units','points','InnerPosition',[100,100,200,200]);
set(gca,'layer','top');
set(gca,'fontsize',10);
xl = get(gca,'XLabel');
set(xl,'FontSize',16);
yl = get(gca,'YLabel');
set(yl,'FontSize',16);
set(gca,'FontName','Myriad Pro')
set(gca,'xminortick','off','yminortick','off');

%% PLOT: ABUNDANCE CHANGE
figthis = figure;
hold on;
box on;
ax = gca;
ax.ActivePositionProperty = 'position';

datathis = mus;
datathis = datathis(:);
datathis = datathis(~isnan(datathis));
xmin = -5;
xmax = 5;
xthis = xmin:1e-5:xmax;
b = nanstd(datathis)/sqrt(2);
expn_this = exp(-abs(xthis)./b)./(2*b);
norm_this = exp(-xthis.^2./(4*b^2))./(2*b*sqrt(pi));
histogram(datathis,xmin:0.1:xmax,...
    'normalization','pdf','edgecolor','none','facealpha',0.5,...
    'DisplayName',['Stdev = ',num2str(b*sqrt(2),2)]);

xbins = xmin:0.1:xmax;
mus_counts = zeros(length(xbins)-1,reps);
for iii = 1:reps
    xthis = mus_pooled(:,((iii-1)*999+1):(iii)*999);
    xthis = xthis(:);
    hthis = histcounts(xthis,xbins,'normalization','pdf');
    mus_counts(:,iii) = hthis;
end
ymean = nanmean(mus_counts,2);
yerrs = nanstd(mus_counts,[],2);
xthis = (xbins(1:end-1)+xbins(2:end))./2;
ycnst = sum(ymean)*mean(diff(xbins));
ymean = ymean./ycnst;
yerrs = yerrs./ycnst;

xthis = [xbins(1),xthis(2:end-1),xbins(end)];
plot(xthis,ymean,'-','linewidth',1.5,'color',ax.ColorOrder(2,:));
xthis = xthis';
xC = [xthis; flipud(xthis)];
yC = [ymean+yerrs; flipud(ymean-yerrs)];
inds = yC<=0;
xC(inds) = [];
yC(inds) = [];
fill(xC, yC, ax.ColorOrder(2,:),...
    'FaceAlpha',0.2,'linestyle','none');

xthis = xmin:1e-5:xmax;
expn_this = exp(-abs(xthis)./b)./(2*b);
plot(xthis,expn_this,'-','linewidth',2,...
    'color',[0,0,0,0.4],...
    'HandleVisibility','off');

set(gca,'yscale','log');
xlabel('Abu change');
ylabel('PDF');
xlim([-4,4]);
ylim([2e-3,2e0]);

set(gca,'linewidth',1,'ticklength',[0.02,0.01]);
set(gcf,'units','points','InnerPosition',[100,100,150,150]);
set(gca,'layer','top');
set(gca,'fontsize',10);
xl = get(gca,'XLabel');
set(xl,'FontSize',16);
yl = get(gca,'YLabel');
set(yl,'FontSize',16);
set(gca,'FontName','Myriad Pro')
set(gca,'xminortick','off','yminortick','off');

%% PLOT: LVAR X LM X
figthis = figure;
hold on;
box on;
ax = gca;
ax.ActivePositionProperty = 'position';

xthis = abunds_lm;
ythis = vars_lm;
inds = isinf(xthis);
ythis(inds) = [];
xthis(inds) = [];
plot(xthis,ythis,'o','markerfacecolor',ax.ColorOrder(1,:),...
    'markersize',4,...
    'HandleVisibility','off');
pthis = polyfit(xthis,ythis,1);
xthis = log10(1e-8):0.1:0;
ythis = polyval(pthis,xthis);

xthis = linspace(-6,0,1e3);
ythis = zeros(length(xthis),reps);
for iii = 1:reps
    indthis = sub2ind([m1,m2,m3,m4,reps],...
        inds_best(1),inds_best(2),inds_best(3),inds_best(4),iii);
    p1this = outputs_all{indthis}{10}(1);
    p0this = outputs_all{indthis}{10}(2);
    ythis(:,iii) = polyval([p1this,p0this],xthis);
end
ythis1 = min(ythis,[],2);
ythis2 = max(ythis,[],2);
ythis = mean(ythis,2);
plot(xthis,ythis,'-','linewidth',1.5,'color',ax.ColorOrder(2,:));
xthis = xthis';
xC = [xthis; flipud(xthis)];
yC = [ythis2; flipud(ythis1)];
fill(xC, yC, ax.ColorOrder(2,:), ...
    'FaceAlpha',0.2,'linestyle','none');

xlabel('Mean abu');
ylabel('Abu variance');
ylim([-10,-1]);
xlim([-6,0]);
xticks([-6,-3,0]);
xticklabels({'10^{-6}','10^{-3}','10^{0}'});
yticks([-9,-5,-1]);
yticklabels({'10^{-9}','10^{-5}','10^{-1}'});

set(gca,'linewidth',1,'ticklength',[0.02,0.01]);
set(gcf,'units','points','InnerPosition',[100,100,150,150]);
set(gca,'layer','top');
set(gca,'fontsize',10);
xl = get(gca,'XLabel');
set(xl,'FontSize',16);
yl = get(gca,'YLabel');
set(yl,'FontSize',16);
set(gca,'FontName','Myriad Pro')
set(gca,'xminortick','off','yminortick','off');

%% PLOT: P(TRES) P(TRET)
figthis = figure;
hold on;
box on;
ax = gca;
ax.ActivePositionProperty = 'position';

min_tres = min(tress_pool(tress_pool>0));
min_tret = min(trets_pool(trets_pool>0));
min_t = min([min_tres,min_tret]);
tress_pool = tress_pool./min_t;
trets_pool = trets_pool./min_t;

bins = [1:1:10,10:2:30,30:3:60,60:4:100,100:5:300];
h1 = histogram(tress_pool,bins,...
    'normalization','pdf','edgecolor','none','facealpha',1,...
    'HandleVisibility','off');
h2 = histogram(trets_pool,bins,...
    'normalization','pdf','edgecolor','none','facealpha',1,...
    'HandleVisibility','off');
h1.Visible = 'off';
h2.Visible = 'off';
xthis = (h1.BinEdges(1:end-1) + h1.BinEdges(2:end))./2;
ythis = h1.Values;
plot(xthis,ythis,'o',...
    'markersize',4,'markeredgecolor','none',...
    'markerfacecolor',ax.ColorOrder(1,:),...
    'DisplayName','t_{res}',...
    'HandleVisibility','off');
ythis = h2.Values;
plot(xthis,ythis,'s',...
    'markersize',3,'markerfacecolor','none',...
    'markeredgecolor',ax.ColorOrder(1,:),...
    'linewidth',1,...
    'DisplayName','t_{ret}',...
    'HandleVisibility','off');
set(gca,'xscale','log','yscale','log');

xthis = logspace(0,2,1e3);
ythis = zeros(length(xthis),reps);
for iii = 1:reps
    indthis = sub2ind([m1,m2,m3,m4,reps],...
        inds_best(1),inds_best(2),inds_best(3),inds_best(4),iii);
    alpha_est = outputs_all{indthis}{21}(1);
    lambda_est = outputs_all{indthis}{21}(2);
    
    c = quadgk(@(t)t.^(-alpha_est).*exp(-lambda_est.*t),...
        1,Inf,'abstol',1e-12,'reltol',1e-12,'MaxIntervalCount',1e4);
    ytemp = ((xthis).^(-alpha_est).*exp(-lambda_est.*xthis))./c;
    ythis(:,iii) = ytemp;
end
ythis1 = min(ythis,[],2);
ythis2 = max(ythis,[],2);
ythis = mean(ythis,2);
plot(xthis,ythis,'-','linewidth',1.5,'color',ax.ColorOrder(2,:));
xthis = xthis';
xC = [xthis; flipud(xthis)];
yC = [ythis2; flipud(ythis1)];
fill(xC, yC, ax.ColorOrder(2,:), ...
    'FaceAlpha',0.2,'linestyle','none');

xthis = logspace(0,2,1e3);
ythis = zeros(length(xthis),reps);
for iii = 1:reps
    indthis = sub2ind([m1,m2,m3,m4,reps],...
        inds_best(1),inds_best(2),inds_best(3),inds_best(4),iii);
    alpha_est = outputs_all{indthis}{23}(1);
    lambda_est = outputs_all{indthis}{23}(2);
    
    c = quadgk(@(t)t.^(-alpha_est).*exp(-lambda_est.*t),...
        1,Inf,'abstol',1e-12,'reltol',1e-12,'MaxIntervalCount',1e4);
    ytemp = ((xthis).^(-alpha_est).*exp(-lambda_est.*xthis))./c;
    ythis(:,iii) = ytemp;
end
ythis1 = min(ythis,[],2);
ythis2 = max(ythis,[],2);
ythis = mean(ythis,2);
plot(xthis,ythis,'-','linewidth',1.5,'color',ax.ColorOrder(3,:));
xthis = xthis';
xC = [xthis; flipud(xthis)];
yC = [ythis2; flipud(ythis1)];
fill(xC, yC, ax.ColorOrder(3,:), ...
    'FaceAlpha',0.2,'linestyle','none');

xlabel('t res');
ylabel('PDF');
ylim([3e-4,1e0]);
xlim([1e0,1e2]);

set(gca,'linewidth',1,'ticklength',[0.02,0.01]);
set(gcf,'units','points','InnerPosition',[100,100,150,150]);
set(gca,'layer','top');
set(gca,'fontsize',10);
xl = get(gca,'XLabel');
set(xl,'FontSize',16);
yl = get(gca,'YLabel');
set(yl,'FontSize',16);
set(gca,'FontName','Myriad Pro')
set(gca,'xminortick','off','yminortick','off');

%% PLOT: P(SLOPES)
figthis = figure;
ax = gca;
hold on;
box on;
ax.ActivePositionProperty = 'position';

edges = -2:0.1:2;
xthis = slopes_log;
histogram(xthis(~isnan(xthis)),edges,'normalization','pdf',...
    'edgecolor','none','facealpha',0.5);
a1 = nanmean(slopes_log);

xlabel('Restoring slope');
ylabel('PDF');
xlim([-2,0]);
ylim([0,3.9]);
set(gca,'fontsize',14,'ticklength',[0.02,0.025]);

xbins = edges;
slopes_counts = zeros(length(xbins)-1,reps);
for iii = 1:reps
    xthis = slopes_log_pooled(:,iii);
    hthis = histcounts(xthis,xbins,'normalization','pdf');
    slopes_counts(:,iii) = hthis;
end
ymean = nanmean(slopes_counts,2);
yerrs = nanstd(slopes_counts,[],2);
xthis = (xbins(1:end-1)+xbins(2:end))./2;
ycnst = sum(ymean)*mean(diff(xbins));
ymean = ymean./ycnst;
yerrs = yerrs./ycnst;

xthis = [xbins(1),xthis(2:end-1),xbins(end)];
plot(xthis,ymean,'-','linewidth',1.5,'color',ax.ColorOrder(2,:));
xthis = xthis';
xC = [xthis; flipud(xthis)];
yC = [ymean+yerrs; flipud(ymean-yerrs)];
fill(xC, yC, ax.ColorOrder(2,:),...
    'FaceAlpha',0.2,'linestyle','none');
yticks([0,1,2,3]);
xticks([-2,-1,0]);

plot([1,1].*a1,[0,3.9],'-','color',ax.ColorOrder(2,:),'linewidth',0.5);

set(gca,'linewidth',1,'ticklength',[0.02,0.01]);
set(gcf,'units','points','InnerPosition',[100,100,150,150]);
set(gca,'layer','top');
set(gca,'fontsize',10);
xl = get(gca,'XLabel');
set(xl,'FontSize',16);
yl = get(gca,'YLabel');
set(yl,'FontSize',16);
set(gca,'FontName','Myriad Pro')
set(gca,'xminortick','off','yminortick','off');

%% PLOT: P(PREVS)
figthis = figure;
hold on;
box on;
ax = gca;
ax.ActivePositionProperty = 'position';

xthis = prevs;
xbins = 0:0.05:1;
xthis(xthis==0) = [];
histogram(xthis,xbins,'normalization','pdf',...
    'edgecolor','none','facealpha',0.5);
xlabel('Prevalence');
ylabel('PDF');
xlim([0,1]);
ylim([0,9]);
set(gca,'fontsize',14,'ticklength',[0.02,0.025]);

prevs_counts = zeros(length(xbins)-1,reps);
for iii = 1:reps
    xthis = prevs_pooled(:,iii);
    hthis = histcounts(xthis,xbins,'normalization','pdf');
    prevs_counts(:,iii) = hthis;
end
ymean = nanmean(prevs_counts,2);
yerrs = nanstd(prevs_counts,[],2);
ycnst = sum(ymean)*mean(diff(xbins));
ymean = ymean./ycnst;
yerrs = yerrs./ycnst;

xthis = (xbins(1:end-1)+xbins(2:end))./2;
xthis = [0,xthis(2:end-1),1];
plot(xthis,ymean,'-','linewidth',1.5);
xthis = xthis';
xC = [xthis; flipud(xthis)];
yC = [smooth(ymean+yerrs); flipud(ymean-yerrs)];
fill(xC, yC, ax.ColorOrder(2,:),...
    'FaceAlpha',0.2,'linestyle','none');
yticks([0,4,8,12]);

set(gca,'linewidth',1,'ticklength',[0.02,0.01]);
set(gcf,'units','points','InnerPosition',[100,100,150,150]);
set(gca,'layer','top');
set(gca,'fontsize',10);
xl = get(gca,'XLabel');
set(xl,'FontSize',16);
yl = get(gca,'YLabel');
set(yl,'FontSize',16);
set(gca,'FontName','Myriad Pro')
set(gca,'xminortick','off','yminortick','off');

%% PLOT: PREVS LM X
figthis = figure;
hold on;
box on;
ax = gca;
ax.ActivePositionProperty = 'position';

xthis = abunds_lm;
ythis = prevs;
plot(xthis,ythis,'o','markerfacecolor',ax.ColorOrder(1,:),...
    'markersize',4);
xlabel('Mean abu');
ylabel('Prevalence');
xlim([-6,0]);
ylim([0,1]);
set(gca,'fontsize',14,'ticklength',[0.02,0.025]);

xthis = abunds_lm_pooled(:);
ythis = prevs_pooled(:);
xbins = -6:0.2:0;
ymean = zeros(length(xbins)-1,1);
yerrs = zeros(length(xbins)-1,1);
for i = 1:length(xbins)-1
    inds = xthis>=xbins(i) & xthis<xbins(i+1);
    ymean(i) = nanmean(ythis(inds));
    yerrs(i) = nanstd(ythis(inds));
end
yerrs(isnan(yerrs)) = 0;
xbins = (xbins(1:end-1)+xbins(2:end))./2;
inds = ~isnan(ymean);
ymean = ymean(inds);
yerrs = yerrs(inds);
xbins = xbins(inds);
plot(xbins,ymean,'-','linewidth',1.5);

xthis = xbins';
xC = [xthis; flipud(xthis)];
yC = [ymean+yerrs; flipud(ymean-yerrs)];
fill(xC, yC, ax.ColorOrder(2,:), ...
    'FaceAlpha',0.2,'linestyle','none');
yticks([0,0.3,0.6,0.9]);
xticks([-6,-4,-2,0]);
xticklabels({'10^{-6}','10^{-4}','10^{-2}','10^0'});

set(gca,'linewidth',1,'ticklength',[0.02,0.01]);
set(gcf,'units','points','InnerPosition',[100,100,150,150]);
set(gca,'layer','top');
set(gca,'fontsize',10);
xl = get(gca,'XLabel');
set(xl,'FontSize',16);
yl = get(gca,'YLabel');
set(yl,'FontSize',16);
set(gca,'FontName','Myriad Pro')
set(gca,'xminortick','off','yminortick','off');

%% PLOT: RANK LM X
figthis = figure;
hold on;
box on;
ax = gca;
ax.ActivePositionProperty = 'position';

xthis = abunds_lm;
xthis(isnan(xthis)) = [];
xthis = sort(xthis,'descend');
plot(xthis,'o','markerfacecolor',ax.ColorOrder(1,:),...
    'markersize',4);
xlabel('Rank');
ylabel('Mean abu');
set(gca,'fontsize',14,'ticklength',[0.02,0.025]);
set(gca,'xscale','log');

abunds_ml_sorted = zeros(N,reps);
for iii = 1:reps
    xthis = abunds_lm_pooled(:,iii);
    xthis = sort(xthis,'descend','missingplacement','last');
    abunds_ml_sorted(:,iii) = xthis;
end
abunds_ml_sorted(isinf(abunds_ml_sorted)) = NaN;

ymean = nanmean(abunds_ml_sorted,2);
yerrs = nanstd(abunds_ml_sorted,[],2);
xthis = 1:length(ymean);
plot(xthis,ymean,'-','linewidth',1.5);

xthis = xthis';
xC = [xthis; flipud(xthis)];
yC = [ymean+yerrs; flipud(ymean-yerrs)];
fill(xC, yC, ax.ColorOrder(2,:), ...
    'FaceAlpha',0.2,'linestyle','none');
yticks([-8,-6,-4,-2,0]);
yticklabels({'10^{-8}','10^{-6}','10^{-4}','10^{-2}','10^{0}'});

set(gca,'linewidth',1,'ticklength',[0.02,0.01]);
set(gcf,'units','points','InnerPosition',[100,100,150,150]);
set(gca,'layer','top');
set(gca,'fontsize',10);
xl = get(gca,'XLabel');
set(xl,'FontSize',16);
yl = get(gca,'YLabel');
set(yl,'FontSize',16);
set(gca,'FontName','Myriad Pro')
set(gca,'xminortick','off','yminortick','off');

%% PLOT: RICHNESS
figthis = figure;
hold on;
box on;
ax = gca;
ax.ActivePositionProperty = 'position';

xthis = sum(relabus_dat>0,1);
xbins = 0:2:100;
histogram(xthis,xbins,'normalization','pdf',...
    'edgecolor','none','facealpha',0.5);
xlabel('Richness');
ylabel('PDF');
xlim([0,40]);
ylim([0,0.14]);
set(gca,'fontsize',14,'ticklength',[0.02,0.025]);

rich_counts = zeros(length(xbins)-1,reps);
for iii = 1:reps
    xthis = rich_pooled(:,iii);
    hthis = histcounts(xthis,xbins,'normalization','pdf');
    rich_counts(:,iii) = hthis;
end
ymean = nanmean(rich_counts,2);
yerrs = nanstd(rich_counts,[],2);
ycnst = sum(ymean)*mean(diff(xbins));
ymean = ymean./ycnst;
yerrs = yerrs./ycnst;

xthis = (xbins(1:end-1)+xbins(2:end))./2;
plot(xthis,ymean,'-','linewidth',1.5);
xthis = xthis';
xC = [xthis; flipud(xthis)];
yC = [smooth(ymean+yerrs); flipud(ymean-yerrs)];
fill(xC, yC, ax.ColorOrder(2,:),...
    'FaceAlpha',0.2,'linestyle','none');
yticks([0,0.04,0.08,0.12]);

plot([1,1].*mean(rich_pooled(:)),[0,1],'-','color',ax.ColorOrder(2,:),'linewidth',0.5);
ylim([0,0.14]);

set(gca,'linewidth',1,'ticklength',[0.02,0.01]);
set(gcf,'units','points','InnerPosition',[100,100,150,150]);
set(gca,'layer','top');
set(gca,'fontsize',10);
xl = get(gca,'XLabel');
set(xl,'FontSize',16);
yl = get(gca,'YLabel');
set(yl,'FontSize',16);
set(gca,'FontName','Myriad Pro')
set(gca,'xminortick','off','yminortick','off');