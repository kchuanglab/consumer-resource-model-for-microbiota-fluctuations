## IMPORT LIBRARIES
import matplotlib.pyplot as plt
import numpy as np
import random
import scipy
from scipy import integrate
import time


## DEFINE CR DYNAMICS (EQUIVALENT TO CR.m)
def CR(t, XY, A):
    N = np.size(A,0)
    M = np.size(A,1)
    X = XY[range(N)]
    Y = XY[range(N,N+M)]
    
    dXdt = np.zeros(N)
    dYdt = np.zeros(M)
    for i in range(N):
        for j in range(M):
            dXdt[i] += X[i]*A[i][j]*Y[j]
            dYdt[j] -= X[i]*A[i][j]*Y[j]
    dXYdt = np.concatenate((dXdt,dYdt))
    return dXYdt


## FIND APPROXIMATE ECOLOGICAL STEADY STATE IN SERIAL DILUTION (EQUIVALENT TO StatFun.m + FindSS.m)
def FindSS(A, Y0):
    #---Define protocol parameters
    N = np.size(A,0);
    t0 = 0
    t1 = 20
    dilute = 200
    reps_max = 20;
    changes_thresh = dilute*0.05;

    #---Initial condition at first cycle with reservoir
    X0 = np.ones(N)/N*100/dilute
    XY0 = np.concatenate((X0,Y0))
    
    #---Iterate until steady
    sol = scipy.integrate.solve_ivp(CR, [t0, t1], XY0, args=(A, ), rtol=1e-6)

    ratios_last = sol.y[range(N),-1]/X0
    for ii in range(reps_max):
        X0 = sol.y[range(N),-1]/dilute
        XY0 = np.concatenate((X0,Y0))
        sol = scipy.integrate.solve_ivp(CR, [t0, t1], XY0, args=(A, ), rtol=1e-6)
        
        ratios_this = sol.y[range(N),-1]/X0
        if np.all(np.absolute(ratios_this - ratios_last) < changes_thresh):
            break
        ratios_last = ratios_this

    return sol


## GENERATE ONE INSTANCE OF A TIME SERIES (EQUIVALENT TO Macro2102.m)
t_start = time.time()

#---Timescale parameters, only for bookkeeping purposes
Amag = 1e-3
Ymag = 1e2
steps = int(1e2)

#---Macroscopic parameters
N = 50
M = 30
S = 0.1
sig = 0.2
k = 0.8

#---Microscopic instance
#-Resource consumption
A = np.random.rand(N, M)
for i in range(N):
    for j in range(M):
        if np.random.rand(1) < S:
            A[i][j] = 0
A = A/np.sum(A)*Amag*N*M*M

#-Resource environment
Y0 = np.random.rand(M)
Y0 = Y0/np.sum(Y0)*Ymag
Y0s = np.zeros([M,steps])
Y0s[:,0] = Y0
for i in range(1,steps):
    Y0s[:,i] = np.abs(Y0s[:,i-1] + np.random.randn(M)*sig*Y0 - k*(Y0s[:,i-1]-Y0))

#---Find steady states
xss = np.zeros([N,steps])
for i in range(steps):
    sol = FindSS(A,Y0s[:,i])
    xss[:,i] = sol.y[range(N),-1]
    print([i,time.time() - t_start])


## PLOT EXAMPLE TIME SERIES
relabus = xss/np.sum(xss,axis=0)
relabus = np.transpose(relabus)
plt.semilogy(relabus)
plt.ylim((1e-4,1e0))
plt.show()