function stat_mat = CollectStat1125(outputs_all,ithis,jthis)
    m1 = 9;
    m2 = 9;
    m3 = 8;
    m4 = 10;
    reps = 20;
    num = m1*m2*m3*m4*reps;
    stats_all = zeros(num,1);
    for ind = 1:num
        othis = outputs_all{ind};
        if ~isempty(othis)
            stats_all(ind) = outputs_all{ind}{ithis}(jthis);
        else
            stats_all(ind) = NaN;
        end
    end
    
    stat_mat = zeros(m1,m2,m3,m4,reps);
    for i1 = 1:m1
        for i2 = 1:m2
            for i3 = 1:m3
                for i4 = 1:m4
                    for i5 = 1:reps
                        indthis = sub2ind([m1,m2,m3,m4,reps],i1,i2,i3,i4,i5);
                        stat_mat(i1,i2,i3,i4,i5) = stats_all(indthis);
                    end
                end
            end
        end
    end
    stat_mat(isinf(stat_mat)) = NaN;
    stat_mat = nanmean(stat_mat,5);
end