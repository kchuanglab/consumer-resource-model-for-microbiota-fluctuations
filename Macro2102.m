function Macro2102(N,M,S,sig,k,rep)

%---Define simulation parameters
steps = 1e3;
Amag = 1e-3;
Ymag = 1e2;

%---Generate random consumption rates
A = rand(N,M);
for i = 1:N
    for j = 1:M
        if rand < S
            A(i,j) = 0;
        end
    end
end
A = A./(sum(A(:))).*(Amag*N*M*M);

%---Generate fluctuating consumption rates
A0 = A;
A0s = zeros(N,M,steps);
A0s(:,:,1) = A0;
for i = 2:steps
    A0s(:,:,i) = abs(A0s(:,:,i-1) + ...
        randn(N,M).*sig.*A0 - k.*(A0s(:,:,i-1) - A0));
end

%---Generate environment
y0 = rand(1,M);
y0 = y0./sum(y0).*Ymag;

%---Find steady states
xss = zeros(steps,N);
tic
for i = 1:steps
    [~,xys] = FindSS(A0s(:,:,i),y0');
    xss(i,:) = xys(end,1:N);
    [i,toc]
end

%---Save output
filename = ['210214_',...
    num2str(N),'_',...
    num2str(M),'_',...
    num2str(S),'_',...
    num2str(sig),'_',...
    num2str(k),'_',...
    num2str(rep),'.mat'];
save(filename,'-v7.3');

end