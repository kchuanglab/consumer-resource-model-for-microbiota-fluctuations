function out = GetCorrs(xthis,ythis)
    inds = isinf(xthis) | isnan(xthis) | isnan(ythis) | isinf(ythis);
    xthis(inds) = [];
    ythis(inds) = [];
    if ~isempty(xthis)
        rp = corr(xthis,ythis,'type','pearson');
        rk = corr(xthis,ythis,'type','kendall');
        rs = corr(xthis,ythis,'type','spearman');
        out = [rp,rk,rs];
    else
        out = [NaN,NaN,NaN];
    end
end