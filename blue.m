function c = blue(m)

if nargin<1
    m = size(get(gcf,'colormap'),1);
end

r = (0:m-1)'/(m-1);
g = r;
b = ones(m,1);
c = flipud([r,g,b]);

end