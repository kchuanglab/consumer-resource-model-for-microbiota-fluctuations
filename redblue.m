function c = redblue(m)

if nargin<1
    m = size(get(gcf,'colormap'),1);
end

m1 = m/2;
r = (0:m1-1)'/(m1-1);
g = r;
r = [r; ones(m1,1)];
g = [g; flipud(g)];
b = flipud(r);
c = [r,g,b];

end