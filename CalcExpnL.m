function out = CalcExpnL(xthis)

    inds = isinf(xthis) | isnan(xthis);
    xthis(inds) = [];

    if length(xthis)>10 && ~all(xthis==0) %~isempty(xthis)
        b = std(xthis)/sqrt(2);
        expn_cdf = zeros(length(xthis),1);
        for i = 1:length(xthis)
            if xthis(i)<0
                expn_cdf(i) = exp(xthis(i)/b)/2;
            else
                expn_cdf(i) = 1 - exp(-xthis(i)/b)/2;
            end
        end
        cdf_mat = sort([xthis,expn_cdf]);
        [~,p,ksstat,~] = kstest(xthis,'cdf',cdf_mat);
        out = [log10(p),ksstat];
    else
        out = [NaN,NaN];
    end

end