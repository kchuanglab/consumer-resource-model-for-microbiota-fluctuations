function [alpha_est, lambda_est, mle] = bplcutfit(h, edges)

a_all = [];
x_all = [];
cs = [];
load('plcut_consts.mat','a_all','x_all','cs');

h = reshape(h,numel(h),1);
edges = reshape(edges,numel(edges),1);

    function fval = plcutoff_mle(p1,p2)
        [~,ind1] = min(abs(a_all - (1-p1)));
        [~,ind2] = min(abs(x_all - p2));
        const = cs(ind1,ind2);

        res = round(p2/0.01);
        pEdges = cs(ind1,[1:1:numel(edges)].*res);

        temp = h.*(log(-diff(pEdges')) - log(const));
        fval = -sum(temp);
    end

a_range = 1.01:0.01:4;
l_range = 0.01:0.01:1;
fvals = zeros(numel(a_range),numel(l_range));
for i = 1:numel(a_range)
    athis = a_range(i);
    for j = 1:numel(l_range)
        fvals(i,j) = plcutoff_mle(athis,l_range(j));
    end
end
[C, I] = min(fvals);
[~, I2] = min(C);
alpha_est = a_range(I(I2));
lambda_est = l_range(I2);
mle  = plcutoff_mle(alpha_est,lambda_est);

end