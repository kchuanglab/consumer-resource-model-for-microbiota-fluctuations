function out = CalcNormL(xthis)

    inds = isinf(xthis) | isnan(xthis);
    xthis(inds) = [];

    if ~isempty(xthis) && ~all(xthis==0)
        b = std(xthis);
        [~,p,ksstat,~] = kstest(xthis./b);
%         norm_cdf = normcdf(xthis,0,b);
%         cdf_mat = sort([xthis,norm_cdf]);
%         [~,p,ksstat,~] = kstest(xthis,'cdf',cdf_mat)
        out = [log10(p),ksstat];
    else
        out = [NaN,NaN];
    end

end