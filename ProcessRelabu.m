function [relabus,tress,trets,mtres,mtret,tress_pool,trets_pool,...
    prevs,abunds_ml,stds_ml,abunds_lm,vars_lm,slopes_log,mus] =...
    ProcessRelabu(relabus)

[N,steps] = size(relabus);
relabus = relabus./sum(relabus);
e_thresh = 1e-4;
for i = 1:steps
    changed = 0;
    while changed==0
        xthis = relabus(:,i);
        inds = xthis<e_thresh;
        if any(xthis(inds)>0)
            xthis(inds) = 0;
            relabus(:,i) = xthis./sum(xthis);
        else
            changed=1;
        end
    end
end

%---Find extinction dynamics
tress = cell(N,1);
trets = cell(N,1);
mtres = zeros(N,1);
mtret = zeros(N,1);
tress_pool = [];
trets_pool = [];
for ii = 1:N
    xthis = relabus(ii,:);
    xthis = xthis>0;

    dxthis = diff(xthis);
    indp = find(dxthis == 1);
    indm = find(dxthis == -1);
    if xthis(1)==1
        if xthis(end)==1
            tres = [indm,steps] - [0,indp];
            if length(tres)>1
                tres = tres(1:end-1);
            end
            tret = indp - indm;
        else
            tres = indm - [0,indp];
            tret = indp - indm(1:end-1);
        end
    else
        if xthis(end)==1
            tres = [indm,steps] - indp;
            if length(tres)>1
                tres = tres(1:end-1);
            end
            tret = indp - [0,indm];
        else
            tres = indm - indp;
            tret = indp - [0,indm(1:end-1)];
        end
    end
    
    tres = tres;
    tret = tret;
    tress{ii} = tres;
    trets{ii} = tret;
    mtres(ii) = mean(tres);
    mtret(ii) = mean(tret);
    tress_pool = [tress_pool,tres];
    trets_pool = [trets_pool,tret];
end

%---Find prevalence
prevs = sum(relabus>0,2)./steps;

%---Find abundances
abunds_ml = zeros(N,1);
stds_ml = zeros(N,1);
for i = 1:N
    xthis = log10(relabus(i,:));
    xthis(isinf(xthis) | isnan(xthis)) = [];
    abunds_ml(i) = mean(xthis);
    stds_ml(i) = std(xthis);
end
abunds_lm = log10(nanmean(relabus,2));
vars_lm = log10(nanvar(relabus,0,2));

%---Find slopes
slopes_log = zeros(N,1);
for ii = 1:N
    xthis = log10(relabus(ii,1:end-1));
    ythis = log10(relabus(ii,2:end))-log10(relabus(ii,1:end-1));
    inds = ~isinf(xthis) & ~isnan(xthis) & ~isinf(ythis) & ~isnan(ythis);
    if sum(inds) > 5 && ...
            length(unique(xthis(inds)))>3 && ...
            length(unique(ythis(inds)))>3
        p = polyfit(xthis(inds),ythis(inds),1);
    else
        p = [NaN,NaN];
    end
    slopes_log(ii) = p(1);
end

%---Find abundance changes
mus = zeros(N,steps-1);
for i = 1:steps-1
    mus(:,i) = log10(relabus(:,i+1)./relabus(:,i));
end
mus(isinf(mus)) = NaN;

end