function [value,isterminal,direction] = StatFun(t,xy,A)
    value = 1;
    if all((abs(CR(0,xy,A)) - 1e-3)<0)
        value = 0;
    end
    isterminal = 1;
    direction = 0;
end