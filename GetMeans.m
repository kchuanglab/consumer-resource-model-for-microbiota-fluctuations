function out = GetMeans(xthis)
    inds = isinf(xthis) | isnan(xthis);
    xthis(inds) = [];
    if ~isempty(xthis)
        out = [mean(xthis),median(xthis),std(xthis),skewness(xthis)];
    else
        out = [NaN,NaN,NaN,NaN];
    end
end