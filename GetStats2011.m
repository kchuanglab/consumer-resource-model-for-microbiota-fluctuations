function outputs = GetStats2011(relabus,filename)

warning('off','all');
[N,steps] = size(relabus);
outputs = {};

%% Process input data
relabus = relabus./sum(relabus);

e_thresh = 1e-4;
for i = 1:steps
    changed = 0;
    while changed==0
        xthis = relabus(:,i);
        inds = xthis<e_thresh;
        if any(xthis(inds)>0)
            xthis(inds) = 0;
            relabus(:,i) = xthis./sum(xthis);
        else
            changed=1;
        end
    end
end

%---Find extinction dynamics
tress = cell(N,1);
trets = cell(N,1);
mtres = zeros(N,1);
mtret = zeros(N,1);
tress_pool = [];
trets_pool = [];
for ii = 1:N
    xthis = relabus(ii,:);
    xthis = xthis>0;

    dxthis = diff(xthis);
    indp = find(dxthis == 1);
    indm = find(dxthis == -1);
    if xthis(1)==1
        if xthis(end)==1
            tres = [indm,steps] - [0,indp];
            if length(tres)>1
                tres = tres(1:end-1);
            end
            tret = indp - indm;
        else
            tres = indm - [0,indp];
            tret = indp - indm(1:end-1);
        end
    else
        if xthis(end)==1
            tres = [indm,steps] - indp;
            if length(tres)>1
                tres = tres(1:end-1);
            end
            tret = indp - [0,indm];
        else
            tres = indm - indp;
            tret = indp - [0,indm(1:end-1)];
        end
    end
    
    tres = tres;
    tret = tret;
    tress{ii} = tres;
    trets{ii} = tret;
    mtres(ii) = mean(tres);
    mtret(ii) = mean(tret);
    tress_pool = [tress_pool,tres];
    trets_pool = [trets_pool,tret];
end

%---Find prevalence
prevs = sum(relabus>0,2)./steps;

%---Find abundances
abunds_ml = zeros(N,1);
stds_ml = zeros(N,1);
for i = 1:N
    xthis = log10(relabus(i,:));
    xthis(isinf(xthis) | isnan(xthis)) = [];
    abunds_ml(i) = mean(xthis);
    stds_ml(i) = std(xthis);
end
abunds_lm = log10(nanmean(relabus,2));
vars_lm = log10(nanvar(relabus,0,2));

%---Find slopes
slopes_log = zeros(N,1);
slopes_nol = zeros(N,1);
for ii = 1:N
    xthis = log10(relabus(ii,1:end-1));
    ythis = log10(relabus(ii,2:end))-log10(relabus(ii,1:end-1));
    
    inds = ~isinf(xthis) & ~isnan(xthis) & ~isinf(ythis) & ~isnan(ythis);
    if sum(inds) > 5 && ...
            length(unique(xthis(inds)))>3 && ...
            length(unique(ythis(inds)))>3
        p = polyfit(xthis(inds),ythis(inds),1);
    else
        p = [NaN,NaN];
    end
    slopes_log(ii) = p(1);
    
    xthis = relabus(ii,1:end-1);
    ythis = relabus(ii,2:end)-relabus(ii,1:end-1);
    
    inds = ~isinf(xthis) & ~isnan(xthis) & ~isinf(ythis) & ~isnan(ythis);
    if sum(inds) > 5 && ...
            length(unique(xthis(inds)))>3 && ...
            length(unique(ythis(inds)))>3
        p = polyfit(xthis(inds),ythis(inds),1);
    else
        p = [NaN,NaN];
    end
    slopes_nol(ii) = p(1);
end

%---Find abundance changes
mus = zeros(N,steps-1);
xms = zeros(N,steps-1);
for i = 1:steps-1
    mus(:,i) = log10(relabus(:,i+1)./relabus(:,i));
    xms(:,i) = (log10(relabus(:,i+1))+log10(relabus(:,i)))./2;
end
mus(isinf(mus)) = NaN;
xms(isinf(xms)) = NaN;

%---Find msd over time
dts = [1:1:floor(steps/2)]';
vars = zeros(length(dts),1);
for j = 1:length(dts)
    dt_this = dts(j);
    mus_this = zeros(N,steps-dt_this);
    for i = 1:steps-dt_this
        mus_this(:,i) = log10(relabus(:,i+dt_this)./relabus(:,i));
    end
    xthis = mus_this;
    xthis = xthis(:);
    xthis = xthis(~isnan(xthis) & ~isinf(xthis));
    vars(j) = var(xthis);
end

%---Calculate autocorr
lags =  zeros(N,1);
for i = 1:N
    acf = autocorr(relabus(i,:),'NumLags',floor(steps/3));
    if ~isempty(find(acf<0,1))
        lags(i) = find(acf<0,1);
    end
end

%---Find plasticity estimate
max_gs = zeros(steps,1);
med_gs = zeros(steps,1);
for t = 1:steps-1
    x0this = relabus(:,t);
    x1this = relabus(:,t+1);

    denom = x1this;
    denom(denom==0) = e_thresh;
    dilus = x0this./denom;
    max_dilu = max(dilus);
    
    denom = x0this;
    denom(denom==0) = e_thresh;
    denom = denom./max_dilu;
    grows = x1this./denom;
    max_gs(t) = log2(max(grows));
    med_gs(t) = log2(median(grows(grows>0)));
end

%% Distribution of abundance and prevalence
outputs{end+1} = GetMeans(abunds_ml);
outputs{end+1} = GetMeans(abunds_lm);
outputs{end+1} = GetMeans(prevs);
outputs{end+1} = GetMeans(log10(prevs));

%% Abundance vs prevalence
xthis = abunds_ml;
ythis = prevs;
outputs{end+1} = [mean(xthis(abs(ythis-0.3)<0.1)),...
    mean(xthis(abs(ythis-0.5)<0.1)),...
    mean(xthis(abs(ythis-0.7)<0.1))];
outputs{end+1} = GetCorrs(xthis,ythis);

xthis = abunds_lm;
ythis = prevs;
outputs{end+1} = [mean(xthis(abs(ythis-0.3)<0.1)),...
    mean(xthis(abs(ythis-0.5)<0.1)),...
    mean(xthis(abs(ythis-0.7)<0.1))];
outputs{end+1} = GetCorrs(xthis,ythis);

%% Abundance vs variance
xthis = abunds_ml;
ythis = stds_ml;
inds = isnan(xthis) | isnan(ythis) | isinf(xthis) | isinf(ythis);
xthis(inds) = [];
ythis(inds) = [];
p = polyfit(xthis,ythis,2);
ypred = polyval(p,xthis);
rms = sqrt(sum((ypred - ythis).^2)/length(ythis));
outputs{end+1} = [p,rms];

xthis = abunds_lm;
ythis = vars_lm;
inds = isnan(xthis) | isnan(ythis) | isinf(xthis) | isinf(ythis);
xthis(inds) = [];
ythis(inds) = [];
coeffs = GetCorrs(xthis,ythis);
p = polyfit(xthis,ythis,1);
ypred = polyval(p,xthis);
rms = sqrt(sum((ypred - ythis).^2)/length(ythis));
outputs{end+1} = [p,coeffs,rms];

%% Rank abundance
xthis = abunds_ml;
xthis(isnan(xthis) | isinf(xthis)) = [];
xthis = sort(xthis,'descend');
p = polyfit(1:length(xthis),xthis',1);
coeffs = GetCorrs([1:length(xthis)]',xthis);
ypred = polyval(p,xthis);
rms = sqrt(sum((ypred - ythis).^2)/length(ythis));
outputs{end+1} = [p,coeffs,rms];

xthis = abunds_lm;
xthis(isnan(xthis) | isinf(xthis)) = [];
xthis = sort(xthis,'descend');
p = polyfit(1:length(xthis),xthis',1);
coeffs = GetCorrs([1:length(xthis)]',xthis);
ypred = polyval(p,xthis);
rms = sqrt(sum((ypred - ythis).^2)/length(ythis));
outputs{end+1} = [p,coeffs,rms];

%% Extinction dynamics t_res, t_ret
outputs{end+1} = GetMeans(mtres);
outputs{end+1} = GetMeans(mtret);
outputs{end+1} = GetMeans(log10(mtres));
outputs{end+1} = GetMeans(log10(mtret));

outputs{end+1} = GetCorrs(abunds_ml,log10(mtres));
outputs{end+1} = GetCorrs(abunds_ml,log10(mtret));
outputs{end+1} = GetCorrs(log10(prevs),log10(mtres));
outputs{end+1} = GetCorrs(log10(prevs),log10(mtret));

bins = 1:1:steps;
xthis = tress_pool;
[h,edges] = histcounts(xthis,bins);
[alpha_est, lambda_est, lpl] = bplcutfit(h,edges);
[mu_est, sigma_est, llg] = blgnormfit(h,edges,[],[]);
outputs{end+1} = [alpha_est,lambda_est,lpl,mu_est,sigma_est,llg];
outputs{end+1} = GetMeans(xthis);

xthis = trets_pool;
[h,edges] = histcounts(xthis,bins);
[alpha_est, lambda_est, lpl] = bplcutfit(h,edges);
[mu_est, sigma_est, llg] = blgnormfit(h,edges,[],[]);
outputs{end+1} = [alpha_est,lambda_est,mu_est,sigma_est,lpl,llg];
outputs{end+1} = GetMeans(xthis);

%% Restoring slopes
outputs{end+1} = GetMeans(slopes_log);
outputs{end+1} = GetMeans(slopes_nol);

outputs{end+1} = GetCorrs(abunds_ml,slopes_log);
outputs{end+1} = GetCorrs(log10(prevs),slopes_log);
outputs{end+1} = GetCorrs(abunds_ml,slopes_nol);
outputs{end+1} = GetCorrs(log10(prevs),slopes_nol);

%% Abundance changes
xthis = mus;
xthis = xthis(:);
outputs{end+1} = GetMeans(xthis);
outputs{end+1} = CalcExpnL(xthis);

outputs{end+1} = GetCorrs(log10(dts),vars);

%% Autocorrs
outputs{end+1} = GetMeans(lags);

%% Growth required
outputs{end+1} = GetMeans(max_gs);
outputs{end+1} = GetMeans(med_gs);

%% Extinction dynamics t_res, t_ret and autocorr, normalized
min_tres = min(tress_pool(tress_pool>0));
min_tret = min(trets_pool(trets_pool>0));
min_t = min([min_tres,min_tret]);

outputs{end+1} = GetMeans(mtres./min_t);
outputs{end+1} = GetMeans(mtret./min_t);
outputs{end+1} = GetMeans(log10(mtres./min_t));
outputs{end+1} = GetMeans(log10(mtret./min_t));

outputs{end+1} = GetCorrs(abunds_ml,log10(mtres./min_t));
outputs{end+1} = GetCorrs(abunds_ml,log10(mtret./min_t));
outputs{end+1} = GetCorrs(log10(prevs),log10(mtres./min_t));
outputs{end+1} = GetCorrs(log10(prevs),log10(mtret./min_t));

bins = 1:1:steps;
xthis = tress_pool./min_t;
[h,edges] = histcounts(xthis,bins);
[alpha_est, lambda_est, lpl] = bplcutfit(h,edges);
[mu_est, sigma_est, llg] = blgnormfit(h,edges,[],[]);
outputs{end+1} = [alpha_est,lambda_est,lpl,mu_est,sigma_est,llg];
outputs{end+1} = GetMeans(xthis);

xthis = trets_pool./min_t;
[h,edges] = histcounts(xthis,bins);
[alpha_est, lambda_est, lpl] = bplcutfit(h,edges);
[mu_est, sigma_est, llg] = blgnormfit(h,edges,[],[]);
outputs{end+1} = [alpha_est,lambda_est,mu_est,sigma_est,lpl,llg];
outputs{end+1} = GetMeans(xthis);

outputs{end+1} = GetMeans(lags./min_t);

%% Richness
nums = sum(relabus>0);
nums_s = std(nums);
nums_m = mean(nums);
outputs{end+1} = [nums_m,nums_s];

%% Save
save(filename,'outputs');

end