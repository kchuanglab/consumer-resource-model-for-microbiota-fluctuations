clear all;
close all;
clc;

%---Load processed simulations
load('201202_all.mat');
N = 50;
Ms = [10,20,30,40,50,100,150,200,250];
Ss = 0.1:0.1:0.9;
sigs = [0.02,0.05,0.1,0.15,0.2,0.3,0.4,0.5];
ks = 0.1:0.1:1;
reps = 20;

m1 = length(Ms);
m2 = length(Ss);
m3 = length(sigs);
m4 = length(ks);

%---Load data
filename = 'CaporasoF4';
load(['CompiledData/',filename,'.mat']);

%---Place threshold on relative abundance
[N,steps] = size(relabus);
relabus = relabus./sum(relabus);
relabus = relabus(:,randperm(steps));
e_thresh = 1e-4;
for i = 1:steps
    changed = 0;
    while changed==0
        xthis = relabus(:,i);
        inds = xthis<e_thresh;
        if any(xthis(inds)>0)
            xthis(inds) = 0;
            relabus(:,i) = xthis./sum(xthis);
        else
            changed=1;
        end
    end
end

%---Calculate time series statistics
data_this = GetStats2011(relabus,'placeholder');

%---Fit by minimizing sum of errors of the 4 uniquely susceptible stats
ithis1 = 50;
jthis1 = 1;
ithis2 = 10;
jthis2 = 1;
ithis3 = 25;
jthis3 = 1;
ithis4 = 31;
jthis4 = 3;
stat_this1 = data_this{ithis1}(jthis1);
stat_this2 = data_this{ithis2}(jthis2);
stat_this3 = data_this{ithis3}(jthis3);
stat_this4 = data_this{ithis4}(jthis4);
stat_mat1 = CollectStat(outputs_all,ithis1,jthis1);
stat_mat2 = CollectStat(outputs_all,ithis2,jthis2);
stat_mat3 = CollectStat(outputs_all,ithis3,jthis3);
stat_mat4 = CollectStat(outputs_all,ithis4,jthis4);

mag1 = nanstd(stat_mat1(:));
mag2 = nanstd(stat_mat2(:));
mag3 = nanstd(stat_mat3(:));
mag4 = nanstd(stat_mat4(:));
d1 = abs((stat_mat1 - stat_this1)./mag1);
d2 = abs((stat_mat2 - stat_this2)./mag2);
d3 = abs((stat_mat3 - stat_this3)./mag3);
d4 = abs((stat_mat4 - stat_this4)./mag4);

err = d1+d2+d3+d4;
[~,ind] = min(err(:));
[i1_best,i2_best,i3_best,i4_best] = ind2sub(size(stat_mat1),ind);

M = Ms(i1_best);
S = Ss(i2_best);
sig = sigs(i3_best);
k = ks(i4_best);

%---Return best fit values
[N,M,S,sig,k]