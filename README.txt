Macro2012.m - Simulates a coarse-grained consumer-resource model with fluctuating resource levels.

FindSS.m - Finds the approximate ecological steady state under a serial dilution scheme.

Fit.m - Fit time series statistics to model predictions.

GenerateOverlay.m - Compares data and model predictions using data from Caporaso et al as an example.

GenerateOverlay_null.m - Compares data to a null non-interacting model.