clear all;
close all;
clc;

%% LOAD DATA
filename = 'CaporasoF4';
load(['CompiledData/',filename,'.mat']);
relabus_dat = relabus;
[relabus_dat,tress,trets,mtres,mtret,tress_pool,trets_pool,...
    prevs,abunds_ml,stds_ml,abunds_lm,vars_lm,slopes_log,mus] =...
    ProcessRelabu(relabus_dat);
load(['CompiledData/Stats/',filename,'.mat']);
outputs_dat = outputs;
[~,steps] = size(relabus_dat);

%% SET PARAMETERS FOR NULL MODEL
N = length(abunds_lm);
ytemp = 10.^vars_lm;
ytemp = sqrt(ytemp);
ytemp = ytemp./(10.^abunds_lm);
ytemp(isnan(ytemp)) = 0;
sig = ytemp';
Ymag = 1e2;
y0 = 10.^abunds_lm';
y0 = y0./sum(y0).*Ymag;
k = 1;

mus_pooled = [];
abunds_lm_pooled = [];
vars_lm_pooled = [];
abunds_ml_pooled = [];
stds_ml_pooled = [];
prevs_pooled = [];
slopes_log_pooled = [];
p0_pooled = [];
p1_pooled = [];
at_pooled = [];
lt_pooled = [];
ar_pooled = [];
lr_pooled = [];
tress_sim_pooled = [];
trets_sim_pooled = [];
mtres_sim_pooled = [];
mtret_sim_pooled = [];
pwcs_sim_pooled = [];
rich_pooled = [];
reps = 5;
tic
for iii = 1:reps
    steps = 1e3;
    y0s = zeros(steps,N);
    y0s(1,:) = y0;
    for i = 2:steps
        y0s(i,:) = abs(y0s(i-1,:) + ...
            randn(1,N).*sig.*y0 - k.*(y0s(i-1,:) - y0));
    end
    relabus = y0s./sum(y0s,2);
    relabus_sim = relabus';

    [relabus_sim,tress_sim,trets_sim,mtres_sim,mtret_sim,...
        tress_pool_sim,trets_pool_sim,...
        prevs_sim,abunds_ml_sim,stds_ml_sim,...
        abunds_lm_sim,vars_lm_sim,slopes_log_sim,mus_sim] =...
        ProcessRelabu(relabus_sim);
    mus_pooled = [mus_pooled,mus_sim];
    abunds_lm_pooled = [abunds_lm_pooled,abunds_lm_sim];
    vars_lm_pooled = [vars_lm_pooled,vars_lm_sim];
    abunds_ml_pooled = [abunds_ml_pooled,abunds_ml_sim];
    stds_ml_pooled = [stds_ml_pooled,stds_ml_sim];
    prevs_pooled = [prevs_pooled,prevs_sim];
    slopes_log_pooled = [slopes_log_pooled,slopes_log_sim];
    rich_pooled = [rich_pooled,sum(relabus_sim>0,1)'];
    
    tress_sim_pooled = [tress_sim_pooled,tress_sim];
    trets_sim_pooled = [trets_sim_pooled,trets_sim];
    mtres_sim_pooled = [mtres_sim_pooled,mtres_sim];
    mtret_sim_pooled = [mtret_sim_pooled,mtret_sim];
    pwcs_sim = zeros(N);
    for i = 1:N
        for j = 1:N
            xthis = relabus_sim(i,:);
            ythis = relabus_sim(j,:);
            xthis = log10(xthis);
            ythis = log10(ythis);
            inds = isinf(xthis) | isnan(xthis) | isinf(ythis) | isnan(ythis);
            xthis(inds) = [];
            ythis(inds) = [];

            if length(xthis)>5
                pwcs_sim(i,j) = corr(xthis',ythis');
            else
                pwcs_sim(i,j) = NaN;
            end
        end
    end
    pwcs_sim_pooled = [pwcs_sim_pooled,pwcs_sim];
    
    xthis = abunds_lm_sim;
    ythis = vars_lm_sim;
    inds = isinf(xthis) | isnan(xthis) | isinf(ythis) | isnan(ythis);
    xthis(inds) = [];
    ythis(inds) = [];
    pthis = polyfit(xthis,ythis,1);
    p0_pooled = [p0_pooled,pthis(2)];
    p1_pooled = [p1_pooled,pthis(1)];
    
    xthis = tress_pool_sim;
    bins = 1:1:steps;
    [h,edges] = histcounts(xthis,bins);
    [alpha_est, lambda_est, ~] = bplcutfit(h,edges);
    ar_pooled = [ar_pooled,alpha_est];
    lr_pooled = [lr_pooled,lambda_est];
    
    xthis = trets_pool_sim;
    [h,edges] = histcounts(xthis,bins);
    [alpha_est, lambda_est, ~] = bplcutfit(h,edges);
    at_pooled = [at_pooled,alpha_est];
    lt_pooled = [lt_pooled,lambda_est];
    [iii,toc]
end

figthis = figure;

%% PLOT: ABUNDANCE CHANGE
subplot(2,4,3);
hold on;
box on;
ax = gca;

datathis = mus;
datathis = datathis(:);
datathis = datathis(~isnan(datathis));
xmin = -5;
xmax = 5;
xthis = xmin:1e-5:xmax;
b = nanstd(datathis)/sqrt(2);
expn_this = exp(-abs(xthis)./b)./(2*b);
norm_this = exp(-xthis.^2./(4*b^2))./(2*b*sqrt(pi));
histogram(datathis,xmin:0.1:xmax,...
    'normalization','pdf','edgecolor','none','facealpha',0.5,...
    'DisplayName',['Stdev = ',num2str(b*sqrt(2),2)]);
set(gca,'yscale','log');
xlabel('Abu change');
ylabel('PDF');
yticks([1e-2,1e-1,1e0]);
yticklabels({'10^{-2}','10^{-1}','10^0'});
xlim([-4,4]);
ylim([2e-3,2e0]);

xbins = xmin:0.1:xmax;
mus_counts = zeros(length(xbins)-1,reps);
for iii = 1:reps
    xthis = mus_pooled(:,((iii-1)*999+1):(iii)*999);
    xthis = xthis(:);
    hthis = histcounts(xthis,xbins,'normalization','pdf');
    mus_counts(:,iii) = hthis;
end
ymean = nanmean(mus_counts,2);
yerrs = nanstd(mus_counts,[],2);
xthis = (xbins(1:end-1)+xbins(2:end))./2;
ycnst = sum(ymean)*mean(diff(xbins));
ymean = ymean./ycnst;
yerrs = yerrs./ycnst;

xthis = [xbins(1),xthis(2:end-1),xbins(end)];
plot(xthis,ymean,'-','linewidth',1.5,'color',ax.ColorOrder(2,:));
xthis = xthis';
xC = [xthis; flipud(xthis)];
yC = [ymean+yerrs; flipud(ymean-yerrs)];
inds = yC<=0;
xC(inds) = [];
yC(inds) = [];
fill(xC, yC, ax.ColorOrder(2,:),...
    'FaceAlpha',0.2,'linestyle','none');

xthis = xmin:1e-5:xmax;
expn_this = exp(-abs(xthis)./b)./(2*b);
plot(xthis,expn_this,'-','linewidth',2,...
    'color',[0,0,0,0.4],...
    'HandleVisibility','off');

%% PLOT: LVAR X LM X
subplot(2,4,2);
hold on;
box on;
ax = gca;

xthis = abunds_lm;
ythis = vars_lm;
inds = isinf(xthis);
ythis(inds) = [];
xthis(inds) = [];
plot(xthis,ythis,'o','markerfacecolor',ax.ColorOrder(1,:),...
    'markersize',4,...
    'HandleVisibility','off');
pthis = polyfit(xthis,ythis,1);
xthis = log10(1e-8):0.1:0;
ythis = polyval(pthis,xthis);

xthis = linspace(-6,0,1e3);
ythis = zeros(length(xthis),reps);
for iii = 1:reps
    p1this = p1_pooled(iii);
    p0this = p0_pooled(iii);
    ythis(:,iii) = polyval([p1this,p0this],xthis);
end
ythis1 = min(ythis,[],2);
ythis2 = max(ythis,[],2);
ythis = mean(ythis,2);
xthis = xthis';
xC = [xthis; flipud(xthis)];
yC = [ythis2; flipud(ythis1)];

xthis = abunds_lm_pooled;
ythis = vars_lm_pooled;
plot(xthis,ythis,'o','markeredgecolor',ax.ColorOrder(2,:),...
    'linewidth',1,'markersize',4);

xlabel('Mean abu');
ylabel('Abu variance');
ylim([-10,-1]);
yticks([-9,-5,-1]);
yticklabels({'10^{-9}','10^{-5}','10^{-1}'})
xlim([-6,0]);
xticks([-6,-3,0]);
xticklabels({'10^{-6}','10^{-3}','10^{0}'})

%% PLOT: P(TRES) P(TRET)
subplot(2,4,7);
hold on;
box on;
ax = gca;
min_tres = min(tress_pool(tress_pool>0));
min_tret = min(trets_pool(trets_pool>0));
min_t = min([min_tres,min_tret]);
tress_pool = tress_pool./min_t;
trets_pool = trets_pool./min_t;

bins = [1:1:10,10:2:30,30:3:60,60:4:100,100:5:300];
h1 = histogram(tress_pool,bins,...
    'normalization','pdf','edgecolor','none','facealpha',1,...
    'HandleVisibility','off');
h2 = histogram(trets_pool,bins,...
    'normalization','pdf','edgecolor','none','facealpha',1,...
    'HandleVisibility','off');
h1.Visible = 'off';
h2.Visible = 'off';
xthis = (h1.BinEdges(1:end-1) + h1.BinEdges(2:end))./2;
ythis = h1.Values;
plot(xthis,ythis,'o',...
    'markersize',4,'markeredgecolor','none',...
    'markerfacecolor',ax.ColorOrder(1,:),...
    'DisplayName','t_{res}',...
    'HandleVisibility','off');
ythis = h2.Values;
plot(xthis,ythis,'s',...
    'markersize',3,'markerfacecolor','none',...
    'markeredgecolor',ax.ColorOrder(1,:),...
    'linewidth',1,...
    'DisplayName','t_{ret}',...
    'HandleVisibility','off');
set(gca,'xscale','log','yscale','log');

xthis = logspace(0,2,1e3);
ythis = zeros(length(xthis),reps);
for iii = 1:reps
    alpha_est = ar_pooled(iii);
    lambda_est = lr_pooled(iii);
    
    c = quadgk(@(t)t.^(-alpha_est).*exp(-lambda_est.*t),...
        1,Inf,'abstol',1e-12,'reltol',1e-12,'MaxIntervalCount',1e4);
    ytemp = ((xthis).^(-alpha_est).*exp(-lambda_est.*xthis))./c;
    ythis(:,iii) = ytemp;
end
ythis1 = min(ythis,[],2);
ythis2 = max(ythis,[],2);
ythis = mean(ythis,2);
plot(xthis,ythis,'-','linewidth',1.5,'color',ax.ColorOrder(2,:));
xthis = xthis';
xC = [xthis; flipud(xthis)];
yC = [ythis2; flipud(ythis1)];
fill(xC, yC, ax.ColorOrder(2,:), ...
    'FaceAlpha',0.2,'linestyle','none');

xthis = logspace(0,2,1e3);
ythis = zeros(length(xthis),reps);
for iii = 1:reps
    alpha_est = at_pooled(iii);
    lambda_est = lt_pooled(iii);
    
    c = quadgk(@(t)t.^(-alpha_est).*exp(-lambda_est.*t),...
        1,Inf,'abstol',1e-12,'reltol',1e-12,'MaxIntervalCount',1e4);
    ytemp = ((xthis).^(-alpha_est).*exp(-lambda_est.*xthis))./c;
    ythis(:,iii) = ytemp;
end
ythis1 = min(ythis,[],2);
ythis2 = max(ythis,[],2);
ythis = mean(ythis,2);
plot(xthis,ythis,'-','linewidth',1.5,'color',ax.ColorOrder(3,:));
xthis = xthis';
xC = [xthis; flipud(xthis)];
yC = [ythis2; flipud(ythis1)];
fill(xC, yC, ax.ColorOrder(3,:), ...
    'FaceAlpha',0.2,'linestyle','none');

xlabel('tres');
ylabel('PDF');
ylim([3e-4,1e0]);
yticks([1e-3,1e-2,1e-1,1e0]);
yticklabels({'10^{-3}','10^{-2}','10^{-1}','10^{0}'})
xlim([1e0,1e2]);
xticks([1e0,1e1,1e2]);
xticklabels({'10^{0}','10^{1}','10^{2}'})

%% PLOT: P(SLOPES)
subplot(2,4,4);
hold on;
box on;
edges = -2:0.1:0;
xthis = slopes_log;
h = histogram(xthis(~isnan(xthis)),edges,'normalization','pdf',...
    'edgecolor','none','facealpha',0.5);
xlabel('s');
ylabel('PDF');
ylim([0,max(h.Values)*1.05]);
xlim([-2,0]);
yticks([0,1,2]);

xbins = edges;
slopes_counts = zeros(length(xbins)-1,reps);
for iii = 1:reps
    xthis = slopes_log_pooled(:,iii);
    hthis = histcounts(xthis,xbins,'normalization','pdf');
    slopes_counts(:,iii) = hthis;
end
ymean = nanmean(slopes_counts,2);
yerrs = nanstd(slopes_counts,[],2);
xthis = (xbins(1:end-1)+xbins(2:end))./2;
ycnst = sum(ymean)*mean(diff(xbins));
ymean = ymean./ycnst;
yerrs = yerrs./ycnst;

xthis = [xbins(1),xthis(2:end-1),xbins(end)];
plot(xthis,ymean,'-','linewidth',1.5,'color',ax.ColorOrder(2,:));
xthis = xthis';
xC = [xthis; flipud(xthis)];
yC = [ymean+yerrs; flipud(ymean-yerrs)];
fill(xC, yC, ax.ColorOrder(2,:),...
    'FaceAlpha',0.2,'linestyle','none');

%% PLOT: P(PREVS)
subplot(2,4,5);
hold on;
box on;
ax = gca;

xthis = prevs;
xthis(xthis==0) = [];
xbins = 0:0.05:1;
h = histogram(xthis,xbins,'normalization','pdf',...
    'edgecolor','none','facealpha',0.5);
xlabel('Prevs');
ylabel('PDF');
xlim([0,1]);
ylim([0,max(h.Values)*1.05]);
yticks([0,2,4,6]);

prevs_counts = zeros(length(xbins)-1,reps);
for iii = 1:reps
    xthis = prevs_pooled(:,iii);
    xthis(xthis==0) = [];
    hthis = histcounts(xthis,xbins,'normalization','pdf');
    prevs_counts(:,iii) = hthis;
end
ymean = nanmean(prevs_counts,2);
yerrs = nanstd(prevs_counts,[],2);
ycnst = sum(ymean)*mean(diff(xbins));
ymean = ymean./ycnst;
yerrs = yerrs./ycnst;

xthis = (xbins(1:end-1)+xbins(2:end))./2;
xthis = [0,xthis(2:end-1),1];
plot(xthis,ymean,'-','linewidth',1.5);
xthis = xthis';
xC = [xthis; flipud(xthis)];
yC = [ymean+yerrs; flipud(ymean-yerrs)];
fill(xC, yC, ax.ColorOrder(2,:),...
    'FaceAlpha',0.2,'linestyle','none');

%% PLOT: PREVS LM X
subplot(2,4,6);
hold on;
box on;
ax = gca;

xthis = abunds_lm;
ythis = prevs;
plot(xthis,ythis,'o','markerfacecolor',ax.ColorOrder(1,:),...
    'markersize',4);
xlabel('Mean abu');
ylabel('Prevs');
xlim([-6,0]);
xticks([-6,-4,-2,0]);
ylim([0,1]);
yticks([0,0.3,0.6,0.9]);

xthis = abunds_lm_pooled(:);
ythis = prevs_pooled(:);
xbins = -6:0.2:0;
ymean = zeros(length(xbins)-1,1);
yerrs = zeros(length(xbins)-1,1);
for i = 1:length(xbins)-1
    inds = xthis>=xbins(i) & xthis<xbins(i+1);
    ymean(i) = nanmean(ythis(inds));
    yerrs(i) = nanstd(ythis(inds));
end
yerrs(isnan(yerrs)) = 0;
xbins = (xbins(1:end-1)+xbins(2:end))./2;
inds = ~isnan(ymean);
ymean = ymean(inds);
yerrs = yerrs(inds);
xbins = xbins(inds);
plot(xbins,ymean,'-','linewidth',1.5);

xthis = xbins';
xC = [xthis; flipud(xthis)];
yC = [ymean+yerrs; flipud(ymean-yerrs)];
fill(xC, yC, ax.ColorOrder(2,:), ...
    'FaceAlpha',0.2,'linestyle','none');

%% PLOT: RANK LM X
subplot(2,4,8);
hold on;
box on;
ax = gca;

xthis = abunds_lm;
xthis(isnan(xthis)) = [];
xthis = sort(xthis,'descend');
plot(xthis,'o','markerfacecolor',ax.ColorOrder(1,:),'markersize',4);
xlabel('Rank');
ylabel('Mean abu');
xticks([1e0,1e1,1e2]);
ylim([-7,0]);
yticks([-6,-4,-2,0]);
yticklabels({'10^{-6}','10^{-4}','10^{-2}','10^{0}'})
set(gca,'xscale','log');

abunds_lm_sorted = zeros(N,reps);
for iii = 1:reps
    xthis = abunds_lm_pooled(:,iii);
    xthis = sort(xthis,'descend','missingplacement','last');
    abunds_lm_sorted(:,iii) = xthis;
end
abunds_lm_sorted(isinf(abunds_lm_sorted)) = NaN;

ymean = nanmean(abunds_lm_sorted,2);
yerrs = nanstd(abunds_lm_sorted,[],2);
ind = min(find(isnan(ymean),1),find(isnan(yerrs),1));
ymean = ymean(1:ind-1);
yerrs = yerrs(1:ind-1);
xthis = 1:length(ymean);
plot(xthis,ymean,'-','linewidth',1.5);

xthis = xthis';
xC = [xthis; flipud(xthis)];
yC = [ymean+yerrs; flipud(ymean-yerrs)];
fill(xC, yC, ax.ColorOrder(2,:), ...
    'FaceAlpha',0.2,'linestyle','none');

%% PLOT: RICHNESS
subplot(2,4,1);
hold on;
box on;
ax = gca;

xthis = sum(relabus_dat>0,1);
xbins = 0:2:100;
histogram(xthis,xbins,'normalization','pdf',...
    'edgecolor','none','facealpha',0.5);
xlabel('Richness');
ylabel('PDF');
xlim([5,35]);
ylim([0,0.15]);
yticks([0,0.05,0.10,0.15]);
set(gca,'fontsize',14,'ticklength',[0.02,0.025]);

rich_counts = zeros(length(xbins)-1,reps);
for iii = 1:reps
    xthis = rich_pooled(:,iii);
    hthis = histcounts(xthis,xbins,'normalization','pdf');
    rich_counts(:,iii) = hthis;
end
ymean = nanmean(rich_counts,2);
yerrs = nanstd(rich_counts,[],2);
ycnst = sum(ymean)*mean(diff(xbins));
ymean = ymean./ycnst;
yerrs = yerrs./ycnst;

xthis = (xbins(1:end-1)+xbins(2:end))./2;
plot(xthis,ymean,'-','linewidth',1.5);
xthis = xthis';
xC = [xthis; flipud(xthis)];
yC = [smooth(ymean+yerrs); flipud(ymean-yerrs)];
fill(xC, yC, ax.ColorOrder(2,:),...
    'FaceAlpha',0.2,'linestyle','none');

set(gca,'linewidth',1,'ticklength',[0.02,0.01]);
set(gcf,'units','points','InnerPosition',[100,100,150,150]);
set(gca,'layer','top');
set(gca,'fontsize',10);
xl = get(gca,'XLabel');
set(xl,'FontSize',16);
yl = get(gca,'YLabel');
set(yl,'FontSize',16);
set(gca,'FontName','Myriad Pro')
set(gca,'xminortick','off','yminortick','off');

%%
set(findobj(gcf,'type','axes'),...
    'FontName','Myriad Pro',...
    'FontSize',10,...
    'TickLength',[0.02,0.02],...
    'LineWidth',0.75);

axs = findobj(gcf,'type','axes');
for j = 1:8
    axthis = axs(j);
    xl = get(axthis,'XLabel');
    set(xl,'FontSize',16);
    yl = get(axthis,'YLabel');
    set(yl,'FontSize',16);
    set(axthis,'xminortick','off','yminortick','off');
end

set(gcf,'units','points','InnerPosition',[20,20,750,400]);