function [ts,xys] = FindSS(A,y0)

%-Define protocol parameters
N = size(A,1);
dilute = 200;
t0 = 0;
t1 = 20;
reps_max = 20;
changes_thresh = dilute*0.05;
ratio_min = dilute*0.95;

%-Define functions
CRThis = @(t,xy)CR(t,xy,A);
StatThis = @(t,xy)StatFun(t,xy,A);
x0 = ones(N,1)./N.*(100/dilute);
xy0 = [x0;y0];

opts = odeset('RelTol',1e-4,'AbsTol',1e-5,...
    'NonNegative',1:length(xy0),'Events',StatThis);

%-Iterate until steady
[~,xys] = ode45(CRThis,[t0,t1],xy0,opts);
ratios_last = xys(end,:)'./xy0;
for ii = 2:reps_max-1
    xy0 = xys(end,:)'./dilute;
    xy0(N+1:end) = y0;
    [~,xys] = ode45(CRThis,[t0,t1],xy0,opts);

    ratios_this = xys(end,:)'./xy0;
    if all(abs(ratios_this(1:N) - ratios_last(1:N)) < changes_thresh)
        break
    end
    ratios_last = ratios_this;
end

%-Solve once more with high precision
opts = odeset('RelTol',1e-8,'AbsTol',1e-9,...
    'NonNegative',1:length(xy0),'Events',StatThis);
xy0 = xys(end,:)'./dilute;
xy0(N+1:end) = y0;
[ts,xys] = ode45(CRThis,[t0,t1],xy0,opts);

end