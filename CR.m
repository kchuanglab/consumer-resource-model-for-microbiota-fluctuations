function dxydt = CR(t,xy,A)

N = size(A,1);
x = xy(1:N);
y = xy(N+1:end);

dxdt = x.*(A*y);
dydt = - y.*(A'*x);
dxydt = [dxdt;dydt];

end